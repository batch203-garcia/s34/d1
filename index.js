//load expressjs module
const express = require("express");

//localhost number
const port = 4000;

//server-
const app = express();

//middleware
app.use(express.json());

//mock data

let users = [{
    username: "TStark3000",
    email: "starkindustries@mail.com",
    password: "notPeterParker"
}, {
    username: "ThorThunder",
    email: "loveAndThunder@mail.com",
    password: "iLoveStromBreaker"
}];

let items = [{
    name: "Mjolnir",
    price: 50000,
    isActive: true
}, {
    name: "Vibranium Shield",
    price: 70000,
    isActive: true
}]

app.get('/', (req, res) => {
    res.send('Hello from my first expressJS API');
});

app.get('/greeting', (req, res) => {
    res.send('Hello from Batch203-Garcia');
});

app.get('/users', (req, res) => {
    res.send(users);
});

//add new user
app.post('/users', (req, res) => {
    console.log(req.body); //result empty
    //creating a new user
    let newUser = {
        username: req.body.username,
        email: req.body.email,
        password: req.body.password
    };
    //adding newUSer
    users.push(newUser);
    console.log(users);
    //updated users
    res.send(users);
});

//delete user -> the last user
app.delete('/users', (req, res) => {
    users.pop();
    res.send(users);
});

//put/update
app.put('/users/:index', (req, res) => {
    console.log(req.body);
    console.log(req.params); //refers to the wild card

    //conversion ['0'] to [0]
    let index = parseInt(req.params.index);
    users[index].password = req.body.password;

    res.send(users[index]);
});

//ACTIVITY

app.get('/items', (req, res) => {
    res.send(items);
});

//add new item
app.post('/items', (req, res) => {
    //creating a new item
    let newItem = {
        name: req.body.name,
        price: req.body.price,
        isActive: req.body.isActive
    };
    items.push(newItem);
    console.log(items);
    res.send(items);
});

//delete 
app.delete('/items', (req, res) => {
    items.pop();
    res.send(items);
});

//put/update
app.put('/items/:index', (req, res) => {
    console.log(req.body);
    console.log(req.params);

    let index = parseInt(req.params.index);
    items[index].name = req.body.name;

    res.send(items[index]);
});

//port listener
app.listen(port, () => console.log(`Server is running at port:${port}`));